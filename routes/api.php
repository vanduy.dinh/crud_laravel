<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FoodSPController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\QtyfoodController;
use Modules\Product\Http\Controllers\ProductController;
use Modules\Details\Http\Controllers\DetailsController;
use App\Http\Controllers\ProductClientController;
use App\Http\Controllers\UserRoleController;
use App\Http\Controllers\ProductDetailsController;
use App\Http\Middleware\CheckAdmin;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Auth::routes();
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//Auth Login + Register
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

//Check Middleware
Route::group(['middleware' => 'AuthSPA'], function () {
    Route::get('/me', [AuthController::class, 'me'])->middleware('auth:sanctum','checkSuperAdmin');
    Route::get('/users',[UsersController::class,'index'])->middleware(('auth:sanctum'));
    Route::post('/me/update', [AuthController::class, 'update'])->middleware(('auth:sanctum'));
    Route::post('/me/dashboard', [AuthController::class, 'index'])->middleware('auth:sanctum');
    Route::put('/me/dashboard/putUser/{id}', [AuthController::class, 'update2'])->middleware('auth:sanctum');
    
    Route::post('/me/product', [ProductController::class, 'index'])->middleware('auth:sanctum');
    Route::post('/me/productdetails', [DetailsController::class, 'index'])->middleware('auth:sanctum');
    // Action User
    Route::get('/me/edituser/{id}', [UsersController::class, 'edit'])->middleware('auth:sanctum');
    Route::put('/me/putedituser/{id}', [UsersController::class, 'update'])->middleware('auth:sanctum');
    //Role
    Route::put('/me/editrole/{id}', [UserRoleController::class, 'update'])->middleware(('auth:sanctum'));
    //product
    Route::get('/me/editProduct/{id}', [ProductController::class, 'edit'])->middleware(('auth:sanctum'));
    Route::put('/me/putProduct/{id}', [ProductController::class, 'update'])->middleware(('auth:sanctum'));
    Route::post('/me/addPro', [ProductController::class, 'store'])->middleware(('auth:sanctum'));
    Route::get('/me/getdetails/{id}', [DetailsController::class, 'edit'])->middleware(('auth:sanctum'));
    Route::put('/me/putDetails/{id}', [DetailsController::class, 'update'])->middleware(('auth:sanctum'));
    Route::post('/me/addDetails', [DetailsController::class, 'store'])->middleware(('auth:sanctum'));
    Route::delete('/me/deleteDetails/{id}', [DetailsController::class, 'destroy'])->middleware(('auth:sanctum'));
    Route::delete('/me/deletePro/{id}', [ProductController::class, 'destroy'])->middleware(('auth:sanctum'));
});

//Client 
Route::group(['middleware'=> 'CheckClient'], function(){
    Route::get('/productclient',[ProductClientController::class,'indexclient'])->middleware(('auth:sanctum'));
    Route::get('/productclient/dev',[DetailsController::class,'indexclient'])->middleware('auth:sanctum','CheckClientDev');
    Route::get('/productclient/prod',[DetailsController::class,'indexclient'])->middleware('auth:sanctum','CheckClientProd');
});

//===============================================================================================================

Route::get('/users/getlist', [UsersController::class, 'getlist'])->middleware('auth:sanctum');
Route::get('/users/{id}', [UsersController::class, 'getitem'])->middleware('auth:sanctum');
// Route::post('/users', [UsersController::class, 'store'])->middleware('auth:sanctum');
Route::post('/users/update', [UsersController::class, 'update'])->middleware('auth:sanctum');
Route::post('/users/delete/{id}', [UsersController::class, 'delete'])->middleware('auth:sanctum');
Route::post('/users/getlistManager', [UsersController::class, 'getlistManager'])->middleware('auth:sanctum');
