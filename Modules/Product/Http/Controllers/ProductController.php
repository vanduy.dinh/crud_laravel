<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Product;
use Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AuthController;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\Guard;
use Modules\Product\Repositories\ProductRepository;
class ProductController extends Controller
{
    use HasRoles;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
 
     protected $ProductRepository;

     public function __construct(
         ProductRepository $ProductRepository
     ) {
         $this->ProductRepository = $ProductRepository;
      
     }

    public function index()
    {
        
        try {
            $data = $this->ProductRepository->index();
            return response()->json($data);
        } catch (\Throwable $th) {
            //throw $th;
        }
        
    }  
    public function thungrac()
    {
        $phantrang2 = Product::orderBy('id_pro','DESC')->paginate(4);
        return response()->json($phantrang2);
    }
    
    public function indexclient()   
    {
        $phantrang2 = Product::orderBy('id_pro','DESC')->with("productdetail")->paginate(44);
        return response()->json($phantrang2);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        
        
        $id = $request->id_user;
        $user = User::find($id);
        $image = $request->file('icon');
        $name =$image->getClientOriginalName();
        if($user->role_id == 1){
         
            $validator = Validator::make($request->all(), [
                'name_pro'=>'required',
                'icon'=>'required',
                'phanloai'=>'required',
                'mota'=>'required|max:10',
            ]);
            //check kiem tra -> add data
                if($validator->fails()){
                    return response()->json($validator->errors()->toJson(),400);
                }else{
                    // dd($request->icon);
                    Product::create([
                                    'name_pro'=>$request->name_pro,
                                    'phanloai'=>$request->phanloai,
                                    'mota'=>$request->mota,
                                    'icon' =>"laravel/basic_cmss/public/images/". $name,
                                ]);
                                return response()->json('them thanh cong');
                }
             return response()->json("data khong dung yeu cau ");
        }
        return response()->json("user khong co quyen nay");
        



           
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_pro)
    {
        //
    }
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_pro)
    {
        // return response()->json("hehe");

        return response()->json(Product::where("id_pro",$id_pro)->first());

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request,$id_pro)
    {
        $user = Auth::user();
        if($user != null){
            $validator = Validator::make($request->all(), [
                'name_pro'=>'required|max:255',
                'phanloai'=>'required|max:255',
                'mota'=>'required|max:255',
            ]);
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(),400);
            }else{
            $prod = Product::where("id_pro",$id_pro)->first();
                if($request->icon ==  null){
                    $prod->update([
                        'name_pro'=>$request->name_pro,
                        'phanloai'=>$request->phanloai,
                        'mota'=>$request->mota,
                    ]);
                }else{
                    $prod->update([
                        'name_pro'=>$request->name_pro,
                        'phanloai'=>$request->phanloai,
                        'mota'=>$request->mota,
                        'icon'=>"laravel/basic_cmss/public/images/".$request->icon,
                    ]);
                }
            
          
            
            return response()->json('update thanh cong');
        }
        return response()->json("du lieu nhap khong dung");

        }

        return response()->json("user khong co quyen");
        

    }


    public function destroy(Request $request,$id_pro)
    {
        Product::where("id_pro",$id_pro)->delete();
        return response()->json('success');
    }

}