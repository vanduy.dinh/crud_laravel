<?php

namespace Modules\Product\Repositories;

use App\Jobs\ClearCache;
use Modules\Core\Exceptions\ApiException;
// use Modules\Core\Repositories\Eloquents\BaseRepository;
use Modules\Partners\Interfaces\PartnersRepositoryInterfaces;
use Modules\Partners\Models\Partners;
use Modules\Partners\Models\Script;
use Storage;
use App\Models\Product;

class ProductRepository
{
    public function __construct()
    {
        // $this->model = $model;
    }
    public function index(){ 
        $phantrang2 = Product::orderBy('id_pro','DESC')->with("productdetail")->paginate(4);
        return $phantrang2;   
    }

}