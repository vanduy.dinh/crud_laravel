<?php

namespace Modules\Details\Http\Controllers;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\Productdetails;
use App\Models\User;
use Validator;
use Modules\Details\Repositories\DetailsRepository;

class DetailsController extends Controller
{
    /**=
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $DetailsRepository;

    public function __construct(
        DetailsRepository $DetailsRepository
    ) {
        $this->DetailsRepository = $DetailsRepository;
    
    }
    public function index()
    {
        try {
            $data = $this->DetailsRepository->index();
            return response()->json($data);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function softdelete(){
    }
    public function indexclient()
    {   
        return response()->json(Productdetails::where('active','1')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {  

        $id = $request->product_id;
        $check = $request->version;
        $validator = Validator::make($request->all(), [
            'link' => 'required',
            'version' => 'required',
            'userpass' => 'required',
            'image' => 'required|image'
        ]);
        if( $validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }
        else{

        //===== check dev and update
        if($check == "dev"){
            $pro = Productdetails::where([['product_id',$id] , ['active','1'] , ['version', 'dev']])->first();
            if($pro){
                $pro->update([
                    'active' => '0',
                ]);

            if($request->hasFile('image')){
                $image = $request->file('image');
                $name =$image->getClientOriginalName();
                $image->move('images/',$name);
                    Productdetails::create([
                        'link'=>$request->link,
                        'version'=>$request->version,
                        'userpass'=>$request->userpass,
                        'product_id'=>$request->product_id,
                        'image'=>"/laravel/basic_cmss/public/images/".$name,
                        'active'=>'1',
                    ]);
                    return response()->json(["message"=>'Upload thanh cong']);
                }
                 return response()->json('Error : upload that bai');
            return response()->json("check dev");

            }
            if($request->hasFile('image')){
                $image = $request->file('image');
                $name =$image->getClientOriginalName();
                $image->move('images/',$name);
                    Productdetails::create([
                        'link'=>$request->link,
                        'version'=>$request->version,
                        'userpass'=>$request->userpass,
                        'product_id'=>$request->product_id,
                        'image'=>"/laravel/basic_cmss/public/images/".$name,
                        'active'=>'1',
                    ]);
                    return response()->json(["message"=>'Upload thanh cong']);
                }
                return response()->json('Error : upload that bai');

                return response()->json("check dev");

        }
        else{
            if($check == "prod"){
                $pro2 = Productdetails::where([['product_id',$id] , ['active','1'] , ['version', 'prod']])->first();
            if($pro2){
                $pro2->update([
                    'active' => '0',
                ]);
                
                if($request->hasFile('image')){
                    $image = $request->file('image');
                    $name =$image->getClientOriginalName();
                    $image->move('images/',$name);
                        Productdetails::create([
                            'link'=>$request->link,
                            'version'=>$request->version,
                            'userpass'=>$request->userpass,
                            'product_id'=>$request->product_id,
                            'image'=>"/laravel/basic_cmss/public/images/".$name,
                            'active'=>'1',
                        ]);
                        return response()->json(["message"=>'Upload thanh cong']);
                    }
                    return response()->json('Error : upload that bai');
            return response()->json("check prod");
            }
            
                if($request->hasFile('image')){
                        $image = $request->file('image');
                        $name =$image->getClientOriginalName();
                        $image->move('images/',$name);
                            Productdetails::create([
                                'link'=>$request->link,
                                'version'=>$request->version,
                                'userpass'=>$request->userpass,
                                'product_id'=>$request->product_id,
                                'image'=>"/laravel/basic_cmss/public/images/".$name,
                                'active'=>'1',
                            ]);
                            return response()->json(["message"=>'Upload thanh cong']);
                        }
                        return response()->json('Error : upload that bai');
                return response()->json("check prod");
            }
        }
        return response()->json("prod");
    }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_prodetail )
    {
        return response()->json(Productdetails::where("id_prodetail",$id_prodetail)->first());
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_prodetail)
    {
        $user = Productdetails::where("id_prodetail",$id_prodetail)->first();
        // dd($user->image);
        if($request->image != $user->image){

            $user->update([
                'link'=>$request->link,
                'userpass'=>$request->userpass,
                'version'=>$request->version,
                'image'=>"/laravel/basic_cmss/public/images/".$request->image,  
            ]);
        } 
        else{
            $user->update([
                'link'=>$request->link,
                'userpass'=>$request->userpass,
                'version'=>$request->version,
                // 'image'=>"/laravel/basic_cmss/public/images/".$request->image,
                
            ]);
        }
        return response()->json('update thanh cong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_prodetail )
    {
        $delete = Productdetails::where("id_prodetail",$id_prodetail)->first();
        $delete->update([
            'active' => '0',
        ]);
        return response()->json('success');
    }
    public function laydev($id_prodetail){
        dd($id_prodetail);
        return response()->json('success');
    }
    public function check_details($id){
        return response()-> json(Productdetails::where("product_id",$id)->get());
          
    }
    
    
}   