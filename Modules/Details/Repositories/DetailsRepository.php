<?php

namespace Modules\Details\Repositories;

use App\Jobs\ClearCache;
use Modules\Core\Exceptions\ApiException;
// use Modules\Core\Repositories\Eloquents\BaseRepository;
use Modules\Partners\Interfaces\PartnersRepositoryInterfaces;
use Modules\Partners\Models\Partners;
use Modules\Partners\Models\Script;
use Storage;
use App\Models\Productdetails;


class DetailsRepository
{

    public function __construct()
    {
      
    }
    public function index(){
        $phantrang2 = Productdetails::where('active','1')->paginate(4);
        return $phantrang2;
    }
    public function store(){
        $id = $request->product_id;
        $check = $request->version;
        $validator = Validator::make($request->all(), [
            'link' => 'required',
            'version' => 'required',
            'userpass' => 'required',
            'image' => 'required|image'
        ]);
        if( $validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }
        else{

        //===== check dev and update
        if($check == "dev"){
            $pro = Productdetails::where([['product_id',$id] , ['active','1'] , ['version', 'dev']])->first();
            $pro->update([
                'active' => '0',
            ]);

            if($request->hasFile('image')){
                    $image = $request->file('image');
                    $name =$image->getClientOriginalName();
                    $image->move('images/',$name);
                        Productdetails::create([
                            'link'=>$request->link,
                            'version'=>$request->version,
                            'userpass'=>$request->userpass,
                            'product_id'=>$request->product_id,
                            'image'=>"/laravel/basic_cmss/public/images/".$name,
                            'active'=>'1',
                        ]);
                        return response()->json(["message"=>'Upload thanh cong']);
                    }
                    return response()->json('Error : upload that bai');

            return response()->json("check dev");
        }
        else{
            if($check == "prod"){
                $pro2 = Productdetails::where([['product_id',$id] , ['active','1'] , ['version', 'prod']])->first();
            
                $pro2->update([
                    'active' => '0',
                ]);

                if($request->hasFile('image')){
                        $image = $request->file('image');
                        $name =$image->getClientOriginalName();
                        $image->move('images/',$name);
                            Productdetails::create([
                                'link'=>$request->link,
                                'version'=>$request->version,
                                'userpass'=>$request->userpass,
                                'product_id'=>$request->product_id,
                                'image'=>"/laravel/basic_cmss/public/images/".$name,
                                'active'=>'1',
                            ]);
                            return response()->json(["message"=>'Upload thanh cong']);
                        }
                        return response()->json('Error : upload that bai');
                return response()->json("check prod");
            }
        }
        return response()->json("prod");
    }
    }

}