<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;


class UsersController extends Controller
{
    //
    public function getlist()
    {
    
        $store_id = request()->user()->store_id;
        if($store_id == null){
            //All User
            $data = User::orderBy('id', 'DESC')->get();
        } else if ($store_id) {
            //User in store_id
            $data = User::where(['store_id'=>$store_id])->orderBy('id', 'DESC')->get();
        }
        return response()->json([
            'status' => true,
            'data' => $data,
        ]);
    }
    public function index()
    {
        $phantrang = User::orderBy('id','DESC')->paginate(4);
        return response()->json($phantrang);
    }
    public function getitem($id)
    {
        $user = User::find($id);
        if (is_null($user)) {
            return response()->json([
                'status' => false,
                'message' => 'User not found',
            ]);
        }
        return response()->json([
            'status' => true,
            'data' => $user,
        ]);
    }

    public function store(Request $request)
    {
        $user = new User;
        try {
            $validatedData = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'username' => 'required|unique:users',
                'password' => 'required|string|min:8',
                'phone' => 'required|unique:users',
            ]);
            if ($validatedData->fails()) {
                return response()->json([
                    'status' => false,
                    'message'  => $validatedData->errors()->first()
                ]);
            }
            $user->name = $request->name;
            $user->username = $request->username;
            $user->password = Hash::make($request->password);
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->role_id = $request->role_id ? $request->role_id : null;
            $user->store_id = $request->store_id ? $request->store_id : null;
            $user->address = $request->address ? $request->address : null;
            $result = $user->save();
            $token = $user->createToken('auth_token')->plainTextToken;
            $user->access_token = $token;
            $user->update();
            if ($result) {
                return response()->json([
                    'status' => true,
                    'message' => 'Create user successfully!',
                ]);
            }
        } catch (\Exception $err) {
            return response()->json([
                'status' => false,
                'message' => $err->getMessage()
            ]);
        }
    }

    public function update2(Request $request)
    {
        $user = User::findOrFail($request->input('id'));
        try {
            $validatedData = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                // 'email' => ['required',Rule::unique('users')->ignore($user->id)],
                'username' => ['required',Rule::unique('users')->ignore($user->id)],
                'phone' => ['required',Rule::unique('users')->ignore($user->id)],
            ]);
            if ($validatedData->fails()) {
                return response()->json([
                    'status' => false,
                    'message'  => $validatedData->errors()->first()
                ]);
            }
            if($request->input('password')){
                $request['password'] = Hash::make($request->input('password'));
            }
            $user->update($request->all());
            return response()->json([
                'status' => true,
                'data' => [
                    'user' => $user,
                ],
                'message' => 'Update successfully!',
            ]);
        } catch (\Exception $err) {
            return response()->json([
                'status' => false,
                'message' => $err->getMessage()
            ]);
        }
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return response()->json([
            'status' => true,
            'message' => 'Delete successfully!',
        ]);
    }

    public function getlistManager(Request $request)
    {
        $store_id = $request->store_id;
        if($store_id == 0){
            //All User
            $data = User::where('role_id','>',1)->orderBy('id', 'DESC')->get();
        } else if ($store_id) {
            //User in store_id
            $data = User::where('store_id','=',$store_id)->where('role_id','>',0)->orderBy('id', 'DESC')->get();
        }
        return response()->json([
            'status' => true,
            'data' => $data,
        ]);
    }

    public function edit($id_pro)
    {   
        return response()->json(User::whereId($id_pro)->first());
    }

    
    public function update(Request $request, $id_pro)
    {
        $user = User::where('id',$id_pro)->first();
        $user->update([
            'name'=>$request->name,
            'email'=>$request->email,
            'phanquyen'=>$request->phanquyen,
            'role_id' => $request->role_id,
        ]);
        return response()->json('update thanh cong');
    }
    public function destroy($id)
    {
        User::whereId($id)->first()->delete();
        return response()->json('success');
    }
    
}



