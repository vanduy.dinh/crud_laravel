<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserRoleController extends Controller     

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
       
         // $this->middleware(['auth:api','role:9|11'], ['except' => ['index', 'store']]  );
         // $this->middleware(['auth:api','role:10'], ['except' => [ 'store']]  );
         
     }
    public function index()
    {
       


///Back up

 $phantrang = User::orderBy('id','DESC')->paginate(4);
        return response()->json($phantrang);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=> bcrypt($request->password)
        ]);
        return response()->json('successfully created');
    }

    /**
     * Display the specified resource.
     * Display the specified resourcce .
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return response()->json(User::whereId($id_pro)->first());

        // if($id_pro == 33 ){
        //     return response()->json(User::whereId($id_pro)->first());

        // }
        // return response()->json("khong co du lieu " );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_pro)
    {
        $id = $request->name;
        $id_1 = $request->email;
        $ddd = $request->role;

        try {
            //code...

            if( Auth::user()->role_id == 1){
                $person = User::where('id',$id_1)->first();
                $person->update([
                    'role_id'=> intval($ddd),
                ]);
                return response()->json('update thanh cong');
             
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
       
      
      
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::whereId($id)->first()->delete();
        return response()->json('success');
    }

}