<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Product extends Authenticatable
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'name_pro','icon' , 'mota' , 'phanloai','id_pro',
    ];
    protected $primaryKey = 'id_pro';
    protected $table = 'product';
    public function productdetail (){
        return $this->hasMany('App\Models\Productdetails','product_id','id_pro');
    }
}
