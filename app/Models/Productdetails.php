<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productdetails extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'image' , 'link' , 'userpass', 'id' ,'product_id' , 'version', 'active',
    ];
    protected $primaryKey = 'id_prodetail';
    protected $table = 'pro_details';
    public function product(){

        return $this->belongsTo('App\Models\Product','product_id','id_prodetail');
        
    }
}
