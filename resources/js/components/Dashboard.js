import React, { useState ,useContext, useEffect } from 'react';
import { Button, Table, Space, Spin,Modal  , Image} from 'antd';
import http from "../http";
import { useNavigate } from "react-router-dom";
import { Col, Drawer, Form, Input, Row, Select } from 'antd';
import { ToastContainer, toast } from 'react-toastify';
import { AppContext } from '../store';
import 'react-toastify/dist/ReactToastify.css';
import { xor } from 'lodash';
const { Option } = Select;
const initPagination = {
  current: 1,
  defaultCurrent: 1,
  total: 5,
  defaultPageSize: 4,
  showSizeChanger: false
}

const Dashboard = () => {
const navigate = useNavigate();  
const { dashboard, updateMe,deleteUser2 } = useContext(AppContext);
const [childrenDrawer, setChildrenDrawer] = useState(false);
const [loading2 , setLoading2] = useState(false)
const [open, setOpen] = useState(false);
const [inputs,setInputs] = useState({});
const [layId,setLayId] = useState([]);
const showDrawer = () => {
  setOpen(true);
};
const onClose = () => {
  setOpen(false);
};
const [form] = Form.useForm();
  const [current, seturlPage] = useState([]);
  const [product, setProduct] = useState([]);
  const [productdetails, setProductdetails] = useState([]);
  const [users, setUsers] = useState([]);
  const [idPro, setIdPro] = useState("");
  const [layid,setLayddsId] = useState([]);
  const [pagination, setPaginate2] = useState(initPagination);
 const edituser = (id_pro) =>{
  navigate('/admin/editUser/'+id_pro.id);
 }


 const rolepermissions = (id_pro) =>{
  navigate('/admin/role_ver/'+id_pro.id);
 }

  const submitX=(id_prodetail) =>{

    showChildrenDrawer()
  }
  const sumbitXem = (id_pro) =>{
    setIdPro(id_pro.id_pro)
    form.setFieldsValue(id_pro)
    showDrawer();
    
  }
  const handletablechange2 = (pagination) => {
    console.log(pagination.current);
    let p = {
      current: pagination.current,
      defaultCurrent: 1,
      total: 10,
      defaultPageSize: 5,
      showSizeChanger: false
    };
    setPaginate2({...p})
    dashboard(pagination.current).then((res)=>{
      setProduct(res.data.data)
    });
  };
 
  const deleteProduct = (id_pro) => {
    var id = id_pro.id;
    deleteUser2(id).then((res)=>{ 

    })
    .catch((err)=>{
      toast("không thành công !!!");
    })
    // http.delete('/users/'+id_pro.id).then(res=>{
    //   fetchAllPro();
    // })
    // .catch((err) =>{
    // toast(" Không Thành Công!");
    // })
  }
//EDIT
  const handleEdit =()=>{
    console.log(layid)
    http.put('/products/update/'+layid,inputs).then((res)=>{
      navigate('/allproduct');
    })
  }

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}
//END EDIT
  const columns = [
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Permission',
      dataIndex: 'role_id',
      render: (phanquyen, record) => {
        if(phanquyen == "1"){
          return (
            <p className='Admincss'>Admin</p>
          );
        } else{
          return (
            <p className='Admincss2'>User</p>
          );
        }
       
      }
    },
    {
      title: 'Action',
      key: 'action',
      render: (id_pro, record) => (
        <Space size="middle">
          <a  className='suadas' onClick={()=> {edituser(id_pro)}}>Sửa</a>
          <a className='suadas' onClick={()=> {rolepermissions(id_pro)}}>Cấp Quyền</a>
          <a className='capquyen' onClick={()=>{deleteProduct(record)}} >Xóa</a>
        </Space>
      ),
    },
  ];

  useEffect(()=>{
    console.log(current)
    setLoading2(true)
    dashboard(current).then((res) => {
      setProduct([...res.data.data]);
    })
    setLoading2(false)

  
  },[]);

  
  const fetchAllPro = (current) => {
    setLoading2(true)
    // http.get('/users'+'?page='+current).then(res=>{
    //   res.data.data.map(items=>{
    //     // console.log("map items",items) 
    //   })
    //     setProduct([...res.data.data]);
    //   setLoading2(false)
    // })
    // })


}

const showChildrenDrawer = (id_pro) => {
  navigate("/allproductdetails")
  setChildrenDrawer(true);
};


const onChildrenDrawerClose = () => {
  setChildrenDrawer(false);
};
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const start = () => {
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
    }, 1000);
  };
  const onSelectChange = (newSelectedRowKeys) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        <h2>DASH BOARD</h2>
        <div
          style={{
            marginBottom: 16,
          }}
        >
          <span
            style={{
              marginLeft: 8,
            }}
          >
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Drawer
          dataSource={productdetails}
          title="INFORMATION"
          width={720}
          onClose={onClose}
          open={open}
          bodyStyle={{
            paddingBottom: 80,
          }}
          extra={
            <Space>
              <Button onClick={onClose}>Cancel</Button>
            </Space>
          }
        >
          <Form form={form} layout="vertical"  >
          <Image
              width={150}
              src={"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRC6iPDSqcgCcAtdEz_rPY0B-sxqMd7hz0Hlg&usqp=CAU"}
            />
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="id"
                  label="ID"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="name"
                  label="Name"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name"  value={inputs.name_pro || ''}  onChange={handleChange} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="email"
                  label="Email"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name=""
                  label="Quyền"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="phanquyen"
                  label="Phân Quyền"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
           
              <Col span={12}>
                
              </Col>
              <Form.Item>
                  <Button type="primary" onClick={handleEdit}>
                    Cập Nhật
                  </Button>
                </Form.Item>
            </Row>
          </Form>
        </Drawer>
        <Spin spinning={loading2}>
        <ToastContainer />

        <Table rowSelection={rowSelection} columns={columns} onChange={handletablechange2}  pagination={pagination}  dataSource={product} rowKey={"id_pro"}/>
            </Spin>
      </div>
    );
  }
 
 
export default Dashboard;
