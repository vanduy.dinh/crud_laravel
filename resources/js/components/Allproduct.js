import React, { useState ,useContext, useEffect } from 'react';
import { Button, Table, Space, Spin,Modal  , Image} from 'antd';
import http from "../http";
import { useNavigate } from "react-router-dom";
import { Col, Drawer, Form, Input, Row, Select } from 'antd';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { AppContext } from '../store';
import CreatePro from '../components/CreateProd';
import { xor } from 'lodash';
const { Option } = Select;
const initPagination = {
  current: 1,
  defaultCurrent: 1,
  total: 1,
  defaultPageSize: 4,
  showSizeChanger: false
}
const AllProduct = () => {
const navigate = useNavigate();
const [childrenDrawer, setChildrenDrawer] = useState(false);
const [loading2 , setLoading2] = useState(false)
const [open, setOpen] = useState(false);
const [inputs,setInputs] = useState({});
const [layId,setLayId] = useState([]);
const { getProduct, deletePro} = useContext(AppContext);

const showDrawer = () => {
  setOpen(true);
};

const onClose = () => {
  setOpen(false);
};

const [form] = Form.useForm();
  const [current, seturlPage] = useState([]);
  const [product, setProduct] = useState([]);
  const [productdetails, setProductdetails] = useState([]);
  const [users, setUsers] = useState([]);
  const [idPro, setIdPro] = useState("");
  const [layid,setLayddsId] = useState([]);
  const [pagination, setPaginate2] = useState(initPagination);
  
  const editdetails = (id_pro)=>{
    navigate('Editprod/'+id_pro.id_pro)
  }
  const submitX=(id_prodetail) =>{
      console.log("hehe",id_prodetail)  
    showChildrenDrawer()
  }

  const sumbitXem = (id_pro) =>{
    setIdPro(id_pro.id_pro)
    form.setFieldsValue(id_pro)
    showDrawer();
    console.log("ss",idPro)
    
  }
 
  const handletablechange2 = (pagination) => {
    console.log(pagination);
    let p = {
      current: pagination.current,
      defaultCurrent: 1,
      total:pagination.total,
      defaultPageSize: 4,
      showSizeChanger: false
    };
    setPaginate2({...p})
    getProduct(pagination.current).then((res)=>{
      console.log(res.data.data)
      setProduct(res.data.data)
    });
  };
  const addDetails = (id_pro) =>{
    navigate('CreateDetails/'+id_pro.id_pro);
    http.store('/productdetails/'+id_pro.id_pro).then(res=>{
    console.log(id_pro.id_pro)
      fetchAllPro();
    })
  }
  const deleteProduct = (id_pro) => {
    deletePro(id_pro.id_pro).then(res=>{
      navigate('/admin/product')
    })
    .catch((err) =>{
    toast("Không Thành Công!");
    })
  }
//EDIT
  const handleEdit =()=>{
    console.log(layid)
    http.put('/products/update/'+layid,inputs).then((res)=>{
      navigate('/allproduct');
    })
  }

  const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}
//Render 
  const columns = [
    {
      title: 'Icon ',
      dataIndex: 'icon',
      render: (_, record) => {
        return (
          <Image src={"http://localhost/"+record.icon} width={50} height={50}/>
               );  
      }
    },
    {
      title: 'Tên Sản Phẩm',
      dataIndex: 'name_pro',
    },
    {
      title: 'Phân Loại',
      dataIndex: 'phanloai',
  
    },
    {
      title: 'Mô tả',
      dataIndex: 'mota',
    },
    {
      title: 'Hành Động',
      key: 'action',
      render: (id_pro, record) => (
        <Space size="middle">
          
          <a className='deletedetail2' onClick={()=> {editdetails(record)}}>Sửa</a>
          <a onClick={()=>{deleteProduct(record)}} ></a>
          <a className='deletedetail2' onClick={()=>{addDetails(record)}}>Thêm Details</a>
          <a className='deletedetail' onClick={()=>{deleteProduct(id_pro)}}>Xóa</a>
        </Space>
      ),
    },
  ];
  useEffect(()=>{
    getProduct(current).then((res) =>{ 
      res.data.data.map(items=>{
               setLayddsId(items.id_pro)
              })
        setProduct(res.data.data);
        setPaginate2({...pagination,defaultPageSize:res.data.per_page ,total : res.data.total })
    })
  },[]);

const fetchPro= ()=>{
  http.get('/productdetails').then(res=>{
    console.log(res.data)
    setProductdetails(res.data)
  })

}
const showChildrenDrawer = (id_pro) => {
  navigate("/allproductdetails")
  setChildrenDrawer(true);
};


const onChildrenDrawerClose = () => {
  setChildrenDrawer(false);
};
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const start = () => {
    setTimeout(() => {
      setSelectedRowKeys([]);
    }, 1000);
  };
  const onSelectChange = (newSelectedRowKeys) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        <div
          style={{
            marginBottom: 16,
          }}
        >
          <span
            style={{
              marginLeft: 8,
            }}
          >
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Drawer
          dataSource={productdetails}
          title="INFORMATION"
          width={720}
          onClose={onClose}
          open={open}
          bodyStyle={{
            paddingBottom: 80,
          }}
          extra={
            <Space>
              <Button onClick={onClose}>Cancel</Button>
            </Space>
          }
        >
          <Form form={form} layout="vertical" >
          <Image src={"http://localhost/laravel/reactjsnew/hehehe/public/images/"} width={150} />
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="id_pro"
                  label="ID"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="name_pro"
                  label="Name"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name"  value={inputs.name_pro || ''}  onChange={handleChange} />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="phanloai"
                  label="Phân Loại"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="mota"
                  label="Mô tả"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
           
              <Col span={12}>
                
              </Col>
              <Form.Item>
                  <Button type="primary" onClick={handleEdit}>
                    Cập Nhật
                  </Button>
                </Form.Item>
            </Row>
          </Form>
        </Drawer>
        <Button  href="http://127.0.0.1:8000/admin/product/CreatePro">THÊM PRODUCT</Button>
        <Spin spinning={loading2}>
        <ToastContainer />

        <Table rowSelection={rowSelection} columns={columns} onChange={handletablechange2}  pagination={pagination}  dataSource={product} rowKey={"id_pro"}/>
            </Spin>
      </div>
    );
  }
export default AllProduct;
