import React, { useState , useEffect  } from 'react';
import { Button, Table, Space,Modal ,Spin, Image} from 'antd';
import http from "../http";
import { DeleteOutlined } from '@ant-design/icons';
import { FloatButton } from 'antd';
import { useNavigate } from "react-router-dom";
import { Col, Drawer, Form, Input, Row, Select ,Divider, Radio} from 'antd';
import { colors } from 'laravel-mix/src/Log';
const { Option } = Select;
const initPagination = {
  current: 1,
  defaultCurrent: 1,
  total: 0,
  defaultPageSize: 5,
  showSizeChanger: false
}
// var someValue = window.localStorage.getItem('user');
// var obj = JSON.parse(someValue); 

const Alldetails = () => {
const navigate = useNavigate(); 

const thungrac =() =>{
  navigate('/ThungRac')
}
const [loading2 , setLoading2] = useState(false)
const [open, setOpen] = useState(false);

const showDrawer = () => {
  setOpen(true);
};
const onClose = () => {
  setOpen(false);
};
const [form] = Form.useForm();
  const [current, seturlPage] = useState([]);
  const [product22, setProduct] = useState([]);
  const [users, setUsers] = useState([]);

  const [pagination, setPaginate2] = useState(initPagination);
  const sumbitXem = (id_pro) =>{
    console.log(id_pro)
    form.setFieldsValue(id_pro)
    showDrawer(); 
  }
  //lấy id trên params
 
  
  const handletablechange2 = (pagination) => {
    let p = {
      current: pagination.current,
      defaultCurrent:pagination.current,
      total:pagination.total,
      defaultPageSize: pagination.defaultPageSize,
      showSizeChanger: true,
    };
  
    setPaginate2({...p})
    fetchAllProdetails(pagination.current);
  };
  const editdetails = (id_prodetail) =>{
    console.log("res id de",id_prodetail.id_prodetail)
    navigate('/Editdetails/'+id_prodetail.id_prodetail)
  }
  const [id_dlte,setDLte] = useState([])

  const deleteProduct = (id_prodetail) => {
    setIsModalOpen(true);
    console.log(id_prodetail.id_prodetail)
    // setDLte(id_prodetail.id_prodetail);
    // http.delete('/productdetails/'+id_prodetail.id_prodetail).then(res=>{
    //     fetchAllProdetails();
    // })
  }
  //MODAL DELETE
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = ({id_prodetail}) => {
    
    http.delete('/productdetails/'+id_prodetail).then(res=>{
      setIsModalOpen(false);
      fetchAllProdetails();
    })
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  
  const columns1 = [
    {
      title: 'Hình  ',
      dataIndex: 'image',
      render: (_, record) => {
        return (
          <Image src={"http://localhost/laravel/reactjsnew/hehehe/public/images/"+record.image} width="50px" height={50}/>
        );
      }
    },
    {
      title: 'Phân Loại',
      dataIndex: 'link',
  
    },
    {
      title: 'Mô tả',
      dataIndex: 'userpass',
      
    },  
    {
      title: 'Phiên Bản',
      dataIndex: 'version',
      render: (version, record) => {

        if(version == "dev"){
          return (
            <p className='Admincss'> DEV</p>
          );
        } else{
          return (
            <p className='Admincss2'> PROD</p>
          );
        }
       
      }
    },
    {
      title: 'Hành Động',
      key: 'action',
      render: (id_pro, record) => (
        <Space size="middle">
          <a className='deletedetail2' onClick={()=>{editdetails(id_pro)}}>Edit</a>
          <a className='deletedetail' onClick={()=>{deleteProduct(id_pro)}} >Delete</a>
          <Modal title="Delete" open={isModalOpen} onOk={() => {handleOk(id_pro)}} onCancel={handleCancel}>
          <p> Bạn Chắc Chắn Muốn Xóa ! </p>
          </Modal>
          </Space>
      ),
    },
  ];
  // bắt đầu phân trang
  useEffect(()=>{
    fetchAllProdetails();
  },[]);
  // end phân trang
  const fetchAllProdetails = (current) => {
    setLoading2(true)
    http.get('/productdetails'+'?page='+current).then(res=>{
        setProduct(res.data.data);
        setLoading2(false)
        console.log(res.data.total)
        var total = res.data.total
        setPaginate2({...pagination,total})
        
    })
}

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const start = () => {
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      
    }, 1000);
  };
  const hasSelected = selectedRowKeys.length > 0;
  const [selectionType, setSelectionType] = useState('checkbox');

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name,
    }),
  };


    return (
      <div>
        <div
          style={{
            marginBottom: 16,
          }}
         >
          <span
            style={{
              marginLeft: 8,
            }}
          >
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Drawer
          title="INFORMATION"
          width={720}
          onClose={onClose}
          open={open}
          bodyStyle={{
            paddingBottom: 80,
          }}
          extra={
            <Space>
              <Button onClick={onClose}>Cancel</Button>
              <Button onClick={onClose} type="primary">
                Submit
              </Button>
            </Space>
          }
        >
          <Form form={form} layout="vertical">
          <Image
              width={150}
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRC6iPDSqcgCcAtdEz_rPY0B-sxqMd7hz0Hlg&usqp=CAU"
            />
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="image"
                  label="Hinh Anh"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
               <Image
               width={200}
               src=""
               />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="phanloai"
                  label="Phân Loại"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="mota"
                  label="Mô tả"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="link"
                  label="Link"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter url',
                    },
                  ]}
                >
                  <Input
                    style={{
                      width: '100%',
                    }}
                    addonBefore="http://"
                    addonAfter=".com"
                    placeholder="Please enter url"
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Drawer>
        <Button className='trask' onClick={thungrac}><DeleteOutlined /> </Button>

        <Spin spinning={loading2}>
        <FloatButton icon={<DeleteOutlined />} onClick={() => thungrac()} />
        


        <Radio.Group
        onChange={({ target: { value } }) => {
          setSelectionType(value);
        }}
        value={selectionType}
      >
        <Radio value="checkbox">Checkbox</Radio>
        <Radio value="radio">radio</Radio>
      </Radio.Group>

      <Divider />


        <Table rowSelection={rowSelection} columns={columns1} onChange={handletablechange2} 
         pagination={pagination}  dataSource={product22} rowKey={"id_pro"}/>
            </Spin>
      </div>
    );
  
};
export default Alldetails; 