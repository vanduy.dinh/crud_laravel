import {
  DesktopOutlined, DownOutlined, HomeOutlined, ProfileOutlined, UserOutlined
} from '@ant-design/icons';
import { Avatar, Dropdown, Layout, Menu } from 'antd';
import React, { useContext, useEffect, useState } from 'react';
import { Link, Outlet } from 'react-router-dom';
import { AppContext } from '../store';
const { Header, Footer, Sider, Content } = Layout
const { SubMenu } = Menu;

const menu = (
  <Menu>
    <Menu.Item key="0">
      <Link to="/">Trang chủ</Link>
    </Menu.Item>
    <Menu.Item key="1">
      <Link to="/me">Thông tin cá nhân</Link>
    </Menu.Item>
  
    <Menu.Item key="3">
      <Link to="/logout">Log out</Link>
    </Menu.Item>
  </Menu>
);

const Cms = () => {
  const { user, getListStores } = useContext(AppContext);
  const [ storeName, setStoreName ] = useState("");
  useEffect(() => {
    // getListStores().then((res) => {
    //   res.data.data.map((i) => {
    //     if(i.id === user.store_id){
    //       setStoreName(i.name);
    //     } 
    //   })
    // })
  },[])
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider
        theme="dark"
        breakpoint="md"
        collapsedWidth="0"
      >
        <Menu mode="inline" defaultSelectedKeys={['0']} defaultOpenKeys={['menus', 'sub1']}>
          <Menu.Item key="0" icon={<DesktopOutlined />}>
            <Link to="/admin">CMS</Link>  
          </Menu.Item>
    
          <Menu.Item key="1" icon={<DesktopOutlined />}>
            <Link to="/admin/product">Product</Link>  
          </Menu.Item>
          <Menu.Item key="2"  icon={<ProfileOutlined />}>
              <Link to="/admin/details">Details</Link>
            </Menu.Item>
            {/* <Menu.Item key="3"  icon={<ProfileOutlined />}>
              <Link to="/admin/trash">Trash</Link>
            </Menu.Item> */}
  

            
        </Menu>
      </Sider>
      <Layout>
        <Header style={{ background: '#fff' }} >
          <div className="login">
            <>
              {user.name}
              <Dropdown overlay={menu} trigger={['click']}>
                <DownOutlined />
              </Dropdown>
            </>
          </div>
        </Header>
        <Content style={{ margin: '24px 16px 0' }}>
          <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
        <Outlet />
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
            DuyDDinh
        </Footer>
      </Layout>
    </Layout>
  )
}

export default Cms