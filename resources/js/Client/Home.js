import React, { useContext, useEffect } from 'react'
import { useNavigate, useSearchParams } from 'react-router-dom';
import { AppContext } from '../store';
import { openNotification } from '../Helpers/Notification';

const Home = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const { momoSuccess } = useContext(AppContext);
  let navigate = useNavigate();
  
  useEffect(()=> {
    let orderId = searchParams.get("orderId")
    let resultCode = searchParams.get("resultCode")
    if(resultCode == 0){
      console.log(orderId)
      momoSuccess({orderId}).then((res)=>{
        openNotification(res.data)
      
      })
    }
  }, [])
  return (
    <>
      
    </>
  )
}

export default Home