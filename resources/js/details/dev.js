import React ,{useEffect , useContext} from 'react';
import { Card, Col, Row,Image,Spin } from 'antd';
import { useNavigate,useParams } from "react-router-dom";
import http from '../http';
import { Space, Table, Tag } from 'antd';
import { useState } from 'react';
import { AppContext } from '../store';
export default function DEV({items}){
  const [loading2 , setLoading2] = useState(false)
  const {getItemsDev} = useContext(AppContext);

  // console.log("data from image",items.id_pro)
  const [getDev , setGetDev] = useState([])
  useEffect(()=>{
    var someValue = window.localStorage.getItem('user');
    var obj = JSON.parse(someValue);
    var id = obj.id;
   // fetchdev(id);
    
    getItemsDev().then((res)=>{
      let data = res.data.filter(index =>(index.version == "dev") )
      let data2 = data.filter(index2=>index2.product_id == items.id_pro)
      setGetDev([...data2])
      setLoading2(false)
    })
  },[]);



    return( 
      <Spin spinning={loading2}>
      
      <div className="site-card-wrapper">
      <Row gutter={6} style={{width:"400px", height:"300px"}}> 
      {
       getDev.map((data, index)=>{

       
          return (
            <Col span={12}  key={index} >
              <Card title="DEV" bordered={true}>
              <ul className='list-group'>
              <li  className="list-group-item  justify-content-center " >
              {/* <Image src={"http://localhost/laravel/basic_cmss/public/images/"+ data.image} width={35} height={35}/> */}
                 </li>
                <li  className="list-group-item" > {data.link}</li>
                <li  className="list-group-item">{data.userpass}</li>
                <li className="list-group-item" >  {data.product_id}</li>
              </ul>
              </Card>
            </Col>
           )
        
       
       })
      }
      </Row>
    </div>
    </Spin>
    )
}