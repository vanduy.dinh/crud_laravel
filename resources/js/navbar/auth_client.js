import React,{ useState , useContext, useEffect}from 'react';
import { Breadcrumb, Layout, Menu, theme, Popover , Tabs, Spin,Popconfirm,message } from 'antd';
import { AppContext } from '../store';
import { Col, Divider, Row} from 'antd';
import http from '../http';
import AuthUser
 from '../components/AuthUser';
import Image222 from '../details/image';
const confirm = (e) => {
  console.log(e);
  message.success('Đăng Xuất Thành Công');
};
const cancel = (e) => {
  console.log(e);
  message.error('Hủy');
};
const { Header, Content, Footer } = Layout;
const Auth_Client = () => {
  const [loading2 , setLoading2] = useState(false)
  var someValue = window.localStorage.getItem('user');
  var obj = JSON.parse(someValue); 
  var email = obj.email;
const {getItemsClient} = useContext(AppContext);
const [icon, setIcon] = useState([]);
const [getID,setId_pro] =  useState([]);
const [details, setDetails] =  useState([]);
const {token,logout} = AuthUser();
const logoutUser = () => {
    if(token != undefined){
        logout();
    }
} 
  useEffect(()=>{
    getItemsClient().then((res)=>{
      setIcon(res.data.data)
      res.data.data.map((items)=>{
        setId_pro(items.id_pro)
      })
    })
  },[]);
  const fetchAllIcon = ()=>{
    setLoading2(true)
    http.get('/productclient').then(res=>{
      setIcon(res.data.data)
      res.data.data.map((items)=>{
      setId_pro(items.id_pro)
    })
    setLoading2(false)

    })
  }
  return (
    <div>   
      <Content
          className="site-layout"
          style={{
            padding: '0 50px',
          }}
        >
    <Spin spinning={loading2}>

      <Row
      style={{paddingTop:50}}
      gutter={[16,16]}>
          {
              icon.map((items, index)=>{
                  return (
                      <Image222 items= {items} index={index} />
                  )
              }
              )
          }
      </Row>
    </Spin>
    </Content>

    </div>
  );
};
export default Auth_Client;